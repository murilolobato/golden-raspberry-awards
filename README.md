# local development environment set up

1. git clone https://gitlab.com/murilolobato/golden-raspberry-awards.git
1. cd golden-raspberry-awards
2. python -m venv venv
3. source venv/bin/activate
4. pip install -r requirements.txt
1. cp .env.dist .env
1. python manage.py migrate
1. python manage.py test
1. python manage.py runserver

The prizes interval endpoint will be available at http://127.0.0.1:8000/prizes-interval/ .

# import a movies file

> The movies are unique by its title.

1. python manage.py import_csv --file="new_movielist.csv"


# create django super user

1. python manage.py createsuperuser


# refresh database

1. rm db.sqlite3
1. python manage.py migrate
1. python manage.py import_csv --file="docs/movielist (2).csv"
1. python manage.py runserver
