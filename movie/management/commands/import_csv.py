from django.core.management import BaseCommand

from movie.utils import MovieFileImporter


class Command(BaseCommand):
    help = 'Importa arquivo CSV de lista de filmes.'

    def add_arguments(self, parser):
        parser.add_argument('--file')

    def handle(self, *args, **options):
        importer = MovieFileImporter()
        result = importer.import_file(options['file'])

        self.stdout.write(result)
