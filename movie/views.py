from django.db.models import Max, Min
from rest_framework import generics, views
from rest_framework.response import Response

from movie.models import Movie, Producer
from movie.serializers import MovieSerializer, ProducerSerializer, PrizeByIntervalSerializer


class MovieList(generics.ListAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer


class ProducerList(generics.ListAPIView):
    queryset = Producer.objects.all()
    serializer_class = ProducerSerializer


class PrizeByIntervalList(views.APIView):
    def get(self, request):
        max_interval__max = Producer.objects.aggregate(Max('max_interval'))['max_interval__max']
        min_interval__min = Producer.objects.aggregate(Min('min_interval'))['min_interval__min']

        prize_by_interval = {
            'min': Producer.objects.filter(min_interval=min_interval__min),
            'max': Producer.objects.filter(max_interval=max_interval__max)
        }
        serializer = PrizeByIntervalSerializer(prize_by_interval)

        return Response(serializer.data)
