from django.test import TestCase
from rest_framework.test import APIClient

from movie.utils import MovieFileImporter


class MovieFileImporterTest(TestCase):
    def test_winner_value_extraction(self):
        """
        Asserts that the string indicating winner or not is correctly parsed from data
        """
        winner_values = ['yes', 'Yes', 'YES', 'yes ', ' yes']
        for value in winner_values:
            self.assertTrue(MovieFileImporter.extract_winner(value))

        not_winner_values = ['', 'no', ' ']
        for value in not_winner_values:
            self.assertFalse(MovieFileImporter.extract_winner(value))

    def test_producers_names_extraction(self):
        """
        Asserts that the string containing the multiple producers names is correctly parsed from data
        """
        raw_producers_list = [
            'Jerry Weintraub',
            'Bill Block, Michael Simkin, Jason Barrett and Barry Josephson',
            'Randy Phillips and Craig Rice',
            'Michael Bay, Ian Bryce, Andrew Form, Bradley Fuller, Scott Mednick and Galen Walker',
            'Debra Hayward, Tim Bevan, Eric Fellner, and Tom Hooper'
        ]

        expected_results = [
            ['Jerry Weintraub'],
            ['Bill Block', 'Michael Simkin', 'Jason Barrett', 'Barry Josephson'],
            ['Randy Phillips', 'Craig Rice'],
            ['Michael Bay', 'Ian Bryce', 'Andrew Form', 'Bradley Fuller', 'Scott Mednick', 'Galen Walker'],
            ['Debra Hayward', 'Tim Bevan', 'Eric Fellner', 'Tom Hooper'],
        ]

        for i in range(len(raw_producers_list)):
            result = MovieFileImporter.extract_producers_names(raw_producers_list[i])
            self.assertListEqual(list(result), expected_results[i])


class PrizeIntervalTest(TestCase):
    def test_request(self):
        client = APIClient()
        response = client.get('/prizes-interval/', format='json')
        self.assertEquals(response.status_code, 200)

    def test_request_content(self):
        client = APIClient()
        response = client.get('/prizes-interval/', format='json')

        self.assertEquals(response.data['min'][0]['producer'], 'Joel Silver')

        self.assertEquals(response.data['max'][0]['producer'], 'Matthew Vaughn')
