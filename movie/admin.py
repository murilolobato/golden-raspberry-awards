from django.contrib import admin

from movie.models import Movie, Producer, MovieProducer


class MovieProducerInline(admin.TabularInline):
    model = MovieProducer


class MovieAdmin(admin.ModelAdmin):
    search_fields = ["id", "title", "year", "studios", ]
    list_display = ["id", "title", "year", "studios", "winner", "producers_names"]
    list_filter = ["winner"]
    inlines = [
        MovieProducerInline,
    ]

    @staticmethod
    def producers_names(obj):
        names = map(lambda mp: mp.producer.name, obj.movieproducers.all())
        return ', '.join(list(names))


class ProducerAdmin(admin.ModelAdmin):
    search_fields = ["id", "name", ]
    list_display = ["id", "name", "min_interval", "max_interval"]
    readonly_fields = ["min_interval", "min_interval_start", "min_interval_end", "max_interval", "max_interval_start",
                       "max_interval_end", ]
    inlines = [
        MovieProducerInline,
    ]


admin.site.register(Movie, MovieAdmin)
admin.site.register(Producer, ProducerAdmin)
