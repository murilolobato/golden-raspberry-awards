import csv

from movie.models import Producer, Movie, MovieProducer


def update_producer_prize_intervals(producer: Producer):
    min_interval = None
    min_interval_start = None
    min_interval_end = None

    max_interval = None
    max_interval_start = None
    max_interval_end = None

    previous_movie = None

    for movieproducer in producer.movieproducers.filter(movie__winner=True).order_by('movie__year').all():
        if previous_movie is not None:

            current_interval = movieproducer.movie.year - previous_movie.year

            if min_interval is None or min_interval > current_interval:
                min_interval = current_interval
                min_interval_start = previous_movie.year
                min_interval_end = movieproducer.movie.year

            if max_interval is None or max_interval < current_interval:
                max_interval = current_interval
                max_interval_start = previous_movie.year
                max_interval_end = movieproducer.movie.year

            producer.min_interval = min_interval
            producer.min_interval_start = min_interval_start
            producer.min_interval_end = min_interval_end
            producer.max_interval = max_interval
            producer.max_interval_start = max_interval_start
            producer.max_interval_end = max_interval_end

            producer.save()

        previous_movie = movieproducer.movie


class MovieFileImporter():
    @staticmethod
    def extract_winner(data):
        return True if data.strip().lower() == 'yes' else False

    @staticmethod
    def extract_producers_names(data):
        producers_names = data.replace(', and ', ' and ').replace(' and ', ',').split(',')
        return map(lambda x: x.strip(), producers_names)

    def import_file(self, filepath):
        with open(filepath) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=';')

            line = 0

            for row in csv_reader:
                line += 1

                if line == 1:
                    continue

                movie = Movie.objects.create(
                    title=row[1],
                    year=int(row[0]),
                    studios=row[2],
                    winner=self.extract_winner(row[4]),
                )

                producers_names = self.extract_producers_names(row[3])
                for producer_name in producers_names:
                    producer = None
                    producer_name = producer_name.strip()

                    if Producer.objects.filter(name=producer_name).exists():
                        producer = Producer.objects.get(name=producer_name)
                    else:
                        producer = Producer.objects.create(name=producer_name)

                    MovieProducer.objects.create(movie=movie, producer=producer)

            return f'{line - 1} Movie(s) imported.'
