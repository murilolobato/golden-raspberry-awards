from rest_framework import serializers

from movie.models import Movie, Producer


class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = ['id', 'title', 'year', 'studios', 'winner']


class ProducerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producer
        fields = ['id', 'name']


class PrizeMinProducerSerializer(serializers.ModelSerializer):
    producer = serializers.SerializerMethodField()
    interval = serializers.SerializerMethodField()
    previousWin = serializers.SerializerMethodField()
    followingWin = serializers.SerializerMethodField()

    class Meta:
        model = Producer
        fields = ['producer', 'interval', 'previousWin', 'followingWin']

    def get_producer(self, obj):
        return obj.name

    def get_interval(self, obj):
        return obj.min_interval

    def get_previousWin(self, obj):
        return obj.min_interval_start

    def get_followingWin(self, obj):
        return obj.min_interval_end


class PrizeMaxProducerSerializer(serializers.ModelSerializer):
    producer = serializers.SerializerMethodField()
    interval = serializers.SerializerMethodField()
    previousWin = serializers.SerializerMethodField()
    followingWin = serializers.SerializerMethodField()

    class Meta:
        model = Producer
        fields = ['producer', 'interval', 'previousWin', 'followingWin']

    def get_producer(self, obj):
        return obj.name

    def get_interval(self, obj):
        return obj.max_interval

    def get_previousWin(self, obj):
        return obj.max_interval_start

    def get_followingWin(self, obj):
        return obj.max_interval_end


class PrizeByIntervalSerializer(serializers.Serializer):
    min = PrizeMinProducerSerializer(many=True)
    max = PrizeMaxProducerSerializer(many=True)
