from django.db import models


class Movie(models.Model):
    title = models.CharField(max_length=200, unique=True)
    year = models.IntegerField()
    studios = models.CharField(max_length=200)
    winner = models.BooleanField()

    def __str__(self):
        return self.title


class Producer(models.Model):
    name = models.CharField(max_length=200, unique=True)

    min_interval = models.IntegerField(null=True, blank=True)
    min_interval_start = models.IntegerField(null=True, blank=True)
    min_interval_end = models.IntegerField(null=True, blank=True)

    max_interval = models.IntegerField(null=True, blank=True)
    max_interval_start = models.IntegerField(null=True, blank=True)
    max_interval_end = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.name


class MovieProducer(models.Model):
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE, related_name='movieproducers')
    producer = models.ForeignKey(Producer, on_delete=models.CASCADE, related_name='movieproducers')
