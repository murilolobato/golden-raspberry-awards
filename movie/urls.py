from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from movie import views

urlpatterns = [
    path('movies/', views.MovieList.as_view()),
    path('producers/', views.ProducerList.as_view()),
    path('prizes-interval/', views.PrizeByIntervalList.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)
