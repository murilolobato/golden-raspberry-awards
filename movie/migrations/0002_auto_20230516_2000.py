from django.db import migrations

from movie.utils import MovieFileImporter
from texo.settings import BASE_DIR


def load_initial_data(apps, schema_editor):
    importer = MovieFileImporter()
    importer.import_file(BASE_DIR / "movie/migrations/movielist.csv")


class Migration(migrations.Migration):

    dependencies = [
        ('movie', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_initial_data),
    ]
