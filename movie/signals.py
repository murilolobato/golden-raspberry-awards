from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from movie.models import MovieProducer
from movie.utils import update_producer_prize_intervals


@receiver(post_save, sender=MovieProducer)
def post_save_movieproducer(sender, instance, created, **kwargs):
    update_producer_prize_intervals(instance.producer)


@receiver(post_delete, sender=MovieProducer)
def post_delete_movieproducer(sender, instance, **kwargs):
    update_producer_prize_intervals(instance.producer)
